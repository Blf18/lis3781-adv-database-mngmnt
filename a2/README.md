> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### Assignment 2 Requirements:

*Two Parts:*

1. Save as lis3781\_a2_solutions.sql 
2. Forward engineer to the CCI server
3. Create blf18 database, and two tables: company and customer
 

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshots of SQL code*

SQL Code 1             |  SQL Code 2
:-------------------------:|:-------------------------:
![Contacts Screenshot](img/code1.png)  |  ![Contacts Information Screenshot](img/code2.png)

*Screenshot of company table*:

![CompanyScreenshot](img/company.png)

*Screenshot of customer table*:

![Customer Screenshot](img/customer.png)





