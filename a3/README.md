> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### Assignment 3 Requirements:

*Two Parts:*

1. Save as lis3781\_a3_solutions.sql 
2. SQL Solutions
3. Create and populate Oracle tables
 

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshots of SQL code*

SQL Code 1             |  SQL Code 2
:-------------------------:|:-------------------------:
![Contacts Screenshot](img/SQLCODE1.png)  |  ![Contacts Information Screenshot](img/SQLCODE2.png)

*Screenshot of Customer Values Table*:

![Customer Screenshot](img/customerv.png)

*Screenshot of Commodity Values Table*:

![Commodity Screenshot](img/commodityv.png)

*Screenshot of Order Values Table*:

![Order Screenshot](img/orderv.png)





