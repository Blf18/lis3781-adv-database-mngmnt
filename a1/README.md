> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Bitbucket repo links:
	
	a) this assignment and
	
	b) the completed tutorial (bitbucketstationlocations).
 

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex.1 SQL Solution
* git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch 
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of A1 ERD*

![A1 ERD Screenshot](img/a1erd.png)

*Screenshot of a1 Ex1*:

![ A1 EX1 Screenshot](img/a1ex1.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Blf18/bitbucketstationlocation/src/master/ "Bitbucket Station Locations")
