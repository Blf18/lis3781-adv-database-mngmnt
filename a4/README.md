> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### Assignment 4 Requirements:

* ERD tables must be populated using MS SQL
* SQL statement questions


#### README.md file should include the following items:

* Screenshot of ERD
* Optional: SQL code for the required reports
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshots of SQL code*

First Half of ERD            |  Second Half of ERD
:-------------------------:|:-------------------------:
![ERD Screenshot](img/erd1.png)  |  ![ERD Information Screenshot](img/erd2.png)







