> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### Project 2 Requirements: 

#### README.md file should include the following items:

- Screenshotof at least one MongoDB shell command(s), (e.g., show collections);
- Optional: JSON code for the required reports.
- Bitbucket repo links	

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshots of Project 2 commands*
![Commands Screenshot](img/com1.png)

*Screenshots of displaying all documents in collection*
![Documents Screenshot](img/com2.png)







