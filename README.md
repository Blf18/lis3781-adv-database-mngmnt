> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Provide screenshot of SQL code
	- Provide screenshot of populated tables
2. [A3 README.md](a3/README.md "My A3 README.md file")
	- Screenshot of SQL code
	- Screenshot of populated tables
	

2. [A4 README.md](a4/README.md "My A4 README.md file")
	* Screenshot of ERD
	* Optional: SQL code for the required reports
	* Bitbucket repo links

2. [A5 README.md](a5/README.md "My A5 README.md file")
	* Screenshot of ERD
	* Optional: SQL code for the required reports
	* Bitbucket repo links


*Projects:*

2. [P1 README.md](p1/README.md "My P1 README.md file")
	- Screenshot of ERD
	- SQL code
	- BitBucket repo links

2. [P2 README.md](p2/README.md "My P2 README.md file")
	- Screenshot of at least one MongoDB shell command(s), (e.g., show collections);
	- Optional: JSON code for the required reports.
	- Bitbucket repo links




