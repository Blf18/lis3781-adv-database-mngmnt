> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Brandt Frazier-Allen

### Project 1 Requirements: 

#### README.md file should include the following items:

* Screenshot of ERD
* Bitbucket repo Links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshots of Project 1 ERD*
![ERD Screenshot](img/p1erd.png)







